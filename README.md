# Quizer :: Time counter

Date | Reviewed | Version | Purpose | Live
---- | -------- | ------- | ------- | ----
2020.04 | 2022.03 | 1.0.0 | Demo | [Quizer](https://demo.elodiebayet.com/quizer)


## Sommaire 
0. [Presentation](#markdown-header-0-presentation)
    * [0.0 - Technologies](#markdown-header-00-technologies)
    * [0.1 - Features](#markdown-header-01-features)
    * [0.2 - Usage](#markdown-header-02-usage)
1. [Structure](#markdown-header-1-structure)
    * [1.0 - Folders and files](#markdown-header-10-folders-and-files)
    * [1.1 - Interface](#markdown-header-11-interface)
2. [Development](#markdown-header-2-development)
3. [Remarks](#markdown-header-3-remarks)

---

## 0 - Presentation

JavaScript Single-Page Application of a counter down for questions. The application counts down a time in seconds depending on the number of questions.


### 0.0 - Technologies

* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript)


### 0.1 - Features

* **Timer interactions** _start, break, restart_
* **Form reset at the end**
* **Integrity of values**


### 0.2 - Usage

This project is just a _demo_. It doesn't fit for public or professionnal usage. Reproduction, public communication, partial or entire of this Website or its content are strictly prohibited without an official and written authorization by the Developer.

---

## 1 - Structure

### 1.0 - Folders and files

* `public/` _public source code for development_
    - `css/` 
    - `js/`
* `assets/`
    - `figures/` _global images or pictures for content integration_
    - `trademark/` _logos, icons, etc. relative to owner / brand_


### 1.1 - Interface

![Screenshot v1.0](./assets/figures/screenshot_v1-0.jpg)

---

## 2 - Development

Architecture pattern in _Model-View-Controller_. JavaScript ES6 syntax.

---

## 3 - Remarks

None.