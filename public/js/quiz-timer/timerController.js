/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.04 + 2022.03
 * @origin :: Belgium, EU
 */

/**
 * Manage and bind View and Data of Quiz Timer
 * @param {Object} view Classe to handle view
 */
class TimerController {

    constructor(view){
        this._view = view;

        // Local values
        this._interval = null;
        this._total = 0;
        this._cache = 0;

        this._view.bindInputValues( this._handleValues );
        this._view.bindControlChrono( this._handleChrono );
        this._view.bindResumeChrono( this._handleResumer );
    }

    /**
     * Manage seconds decrementation until 0 then reset chrono
     * @param {number} totalSeconds 
     * @returns {number}
     */
    _countdown(totalSeconds) {
        totalSeconds--;
        if(totalSeconds === 0) this._resetChrono();
        return totalSeconds;
    }

    /**
     * 
     * @param {number} totalSeconds 
     * @returns 
     */
    _convert(totalSeconds) {
        return {minutes: parseInt(totalSeconds / 60), seconds: totalSeconds % 60};
    }
    
    /**
     * Prepare chrono with computed timer and cached value
     * @param {number} totalSeconds 
     */
    _setupChrono = totalSeconds => {
        this._view.displayChrono( this._convert( totalSeconds ) );
        this._cache = this._total;
    }
    
    /**
     * Set chrono to its inital state
     */
    _resetChrono = () => {
        clearInterval(this._interval);
        this._total = 0;
        this._view.stateReset();
        this._setupChrono(0);
    }

    _handleValues = (questions, delay) => { 
        this._total = questions * delay;
        this._setupChrono(this._total);
    }
    _handleChrono = state => {
        this._view.stateRun(state);
        if (state) {
            this._interval = setInterval(() => { 
                this._total = this._countdown(this._total);
                this._view.displayChrono( this._convert( this._total ) );
            }, 1000 );
        } else {
            clearInterval(this._interval);
        }
    }
    _handleResumer = () => {
        clearInterval(this._interval);
        this._total = this._cache;
        this._view.stateResume();
        this._setupChrono(this._total);
    }
}

export default TimerController;