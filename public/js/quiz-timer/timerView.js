/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.04 + 2022.03
 * @origin :: Belgium, EU
 */

/**
 * Handle HTML display for Tasks List mangement
 * @param {Object} form HTML form element to handle values
 * @param {Object} list HTML element where display computed time
 */
class TimerView {
    
    constructor(form, chrono) {

        this._form = form;
        this._chrono = chrono;

        // Local node queries
        this._fields = form.querySelectorAll('input[type="number"]');
        this._control = form.querySelector('#control');
        this._resume = form.querySelector('#resume');
        this._minutes = chrono.querySelector('#minutes');
        this._seconds = chrono.querySelector('#seconds');
        
        // Local storage for values
        this._questions = 0;
        this._delay = 0;

        // Init
        this._uiEventsManager();
    }

    _uiEventsManager() {

        this.stateReset();

        this._form.addEventListener('input', evt => {
            
            this[`_${evt.target.name}`] = this._constraint(evt.target.value, evt.target.min, evt.target.max);
            
            if (this._questions > 0 && this._delay > 0) {
                this._control.disabled = false;
            } else {
                this._control.disabled = true;
            }
        });
    }
    
    /**
     * Check value for minutes or seconds
     * @param {number} value Current minutes or seconds
     * @param {number} min Minimum value constrained by field
     * @param {number} max Mximum value constrained by fiel
     * @returns {number} Current value if valid or 0 if not
     */
    _constraint(value, min, max) {
        value = parseInt(value);
        return (value >= min && value <= max )? value : 0;
    }

    /**
     * Display correct format of minutes or seconds if under 10
     * @param {number} value Current minutes or seconds
     * @returns {string}
     */
    _formatValue(value) {
        return (value < 10)? `0${value}` : `${value}`;
    }

    /**
     * Reset all current timer
     */
    stateReset() {
        this._fields.forEach(field => { field.readOnly = false; field.value = '' });
        this._resume.disabled = true;
        this._control.name = 'run';
        this._control.value = 'Lancer';
        this._control.disabled = true;
        this._chrono.classList.remove('run', 'break');
        this._chrono.classList.add('wait');
    }

    /**
     * Resume the status of current timer
     */
     stateResume() {
        this._resume.disabled = true;
        this._control.name = 'run';
        this._control.value = 'Lancer';
        this._chrono.classList.remove('run', 'break');
        this._chrono.classList.add('wait');
    }
    
    /**
     * Switch status of current timer between running or paused
     * @param {boolean} state True if chrono is running or false if paused
     */
    stateRun(state) {
        this._fields.forEach(field => { field.readOnly = true });
        this._resume.disabled = false;
        this._control.name = (state)? 'break' : 'run';
        this._control.value = (state)? 'Interrompre' : 'Relancer';
        this._chrono.classList.remove('wait', 'run', 'break');
        this._chrono.classList.add( (state)? 'run' : 'break');
    }

    /**
     * Display Timer
     * @param {Object} counter Contains values of 'minutes' and 'seconds'
     */
    displayChrono(counter) {
        this._minutes.textContent = this._formatValue(counter.minutes);
        this._seconds.textContent = this._formatValue(counter.seconds);
    }

    bindInputValues(handler) {
        this._form.addEventListener('input', evt => {
            if (this._questions > 0 && this._delay > 0) {
                handler( this._questions, this._delay );
            }
        });
    }

    bindControlChrono(handler) {
        this._control.addEventListener('click', evt => {
            handler(evt.target.name === 'run');
        });
    }

    bindResumeChrono(handler) {
        this._resume.addEventListener('click', evt => {
            handler();
        });
    }
}

export default TimerView;