'use strict';
/**
 * @author :: Elodie Bayet
 *   @role :: Fullstack Web Developer
 *   @date :: 2020.04 + 2022.03
 * @origin :: Belgium, EU
 */

import TimerController from './quiz-timer/timerController.js';
import TimerView from './quiz-timer/timerView.js';

(() => {
	const form = document.querySelector('form[name="quizform"]');
	const chrono = document.querySelector('#output .chrono');

	const view = new TimerView(form, chrono);

	const Timer = new TimerController(view);
})();